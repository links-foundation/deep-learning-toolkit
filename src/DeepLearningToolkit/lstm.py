from tensorflow import keras
import numpy as np
import json
from pathlib import Path
from typing import Tuple
import datetime
import os.path
import logging
import logging.config

logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)

class LSTM(object):

    def __init__(self, name:str, model_dir:str, metrics_dir:str):
        """ Constructor. Initialize the model to None and set the model's name.
        """
        self.model = None
        self.name = name
        self.model_dir = model_dir
        self.metrics_dir = metrics_dir

    def create_model(self, n_ts_in:int, n_features:int):
        """ Create a new model from scratch.
        """
        raise NotImplementedError

    def save_model(self):
        """ Save current model's architecture and weights.
        """
        path = self.model_dir + "/" + self.name

        # create folder for the model, if it does not exist
        Path(path).mkdir(parents=True, exist_ok=True)

        # save model's architecture
        model_json_dict = json.loads(self.model.to_json())

        with open(path + "/" + self.name + ".json", "w") as output_file:
            json.dump(model_json_dict, output_file)

        # save model's weights
        self.save_model_weights()

        return

    def save_model_weights(self):
        """ Save current model's weights.
        """
        path = self.model_dir + "/" + self.name
        self.model.save_weights(path + "/" + self.name + ".hdf5")

        return

    def load_model(self) -> Tuple[int,int]:
        """ Load an already existing model.
            Return parameters (n_ts_in, n_features), which are retrieved from the loaded model.
        """

        path = self.model_dir + "/" + self.name
        n_ts_in = 0
        n_features = 0

        # load model's architecture
        with open(path + "/" + self.name + ".json", "r") as input_file:
            model_json_dict = json.load(input_file)
        
        for layer in model_json_dict["config"]["layers"]:
            if layer["class_name"] == "InputLayer":
                batch_input_shape = layer["config"]["batch_input_shape"]
                n_ts_in = batch_input_shape[1]
                n_features = batch_input_shape[2]

        self.model = keras.models.model_from_json(json.dumps(model_json_dict))
        logger.info("Loaded architecture")

        # load model's weights
        weights_path = path + "/" + self.name + ".hdf5"
        if os.path.isfile(weights_path):
            self.model.load_weights(weights_path)
            logging.info("Loaded weights")

        # create folder for the metrics, if it does not exist
        path = self.metrics_dir + "/" + self.name
        Path(path).mkdir(parents=True, exist_ok=True)

        # create csv file to save metrics values
        metrics_file = path + "/" + self.name + "_metrics.csv"
        if not os.path.isfile(metrics_file):
            with open(metrics_file, "w") as output_file:
                output_file.write("timestamp,loss,accuracy\n")

        self.model.compile(loss="categorical_crossentropy",
                optimizer=keras.optimizers.Adam(lr=0.01),
                metrics=["accuracy"])

        # self.model.summary()

        return n_ts_in, n_features

    def train_batch(self, x_batch: np.ndarray, y_batch: np.ndarray) -> dict:
        """ Run a single gradient update on the batch of data that is passed as parameter.
            Return a dictionary with the values of the metrics.
        """
        history = self.model.train_on_batch(x_batch, y_batch)

        loss = history[0]
        accuracy = history[1]
        metrics_dict = {}
        metrics_dict["loss"] = loss
        metrics_dict["accuracy"] = accuracy

        return metrics_dict

    def predict(self, x: np.ndarray) -> Tuple[int,float]:
        """ Generate predictions for one single input sample.
            Return result and precision.
        """
        prediction = self.model.predict(x, batch_size=1)

        probability_0 = prediction[0,0]
        probability_1 = prediction[0,1]

        if probability_0 >= probability_1:
            prediction_result = int(0)
            prediction_precision = probability_0
        else:
            prediction_result = int(1)
            prediction_precision = probability_1

        return prediction_result, prediction_precision

    def save_metrics(self, metrics_dict: dict):
        """ Save metrics to CSV file with the corresponding metrics history.
        """
        path = self.metrics_dir + "/" + self.name

        # UTC+0 timestamp
        timestamp = datetime.datetime.utcnow().isoformat()

        metrics_string = timestamp + "," + str(metrics_dict["loss"]) + "," + str(metrics_dict["accuracy"]) + "\n"

        with open(path + "/" + self.name + "_metrics.csv", "a+") as output_file:
            output_file.write(metrics_string)

        return







