import sys
import logging
import logging.config
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from typing import List, Tuple
from pandas.api.types import CategoricalDtype

sys.path.append("src/")
from DataModel import Measurement

# LOGGING DEFINITION
logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


class Processing(object):

    def __init__(self):
        """ Constructor. Initialize the list of features' names and limits.
        """
        self.df_features = None
        self.list_ordered_features = []
        self.timestamp_threshold = 0
        self.is_label = False
    
    def _measurement_to_dataframe(self, measurement: Measurement) -> pd.DataFrame:
        """ Insert the data related to a single measurement (corresponding to a time interval)
            into a dataframe with columns = ["name", "value", "timestamp"]. Example:
        
            name          value     timestamp
        0   temperature1  21.5  2022-11-13T16:14:30.619824+00:00
        1   temperature2  21.7  2022-11-13T16:14:30.619824+00:00
        ...

        The measurement dataframe is returned.

        """

        measurement_dict = measurement.dict()

        column_names = ["name", "value", "timestamp"]
        df_measurement = pd.DataFrame(columns = column_names)

        for datastream_dict in measurement_dict["Datastreams"]:

            if datastream_dict["Observations"] != None:
                
                observations_list = datastream_dict["Observations"]

                # take the mean value of the measured quantity over the considered interval
                # keep the timestamp of the oldest observation (the utilized format for the timestamp is the ISO 8601 standard)
                values = np.array([])
                list_timestamp_obj = []
                
                for observation_dict in observations_list:
                    
                    values = np.append(values, [float(observation_dict["result"])])
                    datetime_obj = datetime.fromisoformat(observation_dict["resultTime"])
                    list_timestamp_obj.append(datetime_obj)

                mean_value = np.mean(values)
                datetime_obj_timestamp = min(list_timestamp_obj)
            
            else:
                datetime_obj_timestamp = None
                mean_value = None

            df_measurement = df_measurement.append({"name": datastream_dict["name"],
                                                    "value": mean_value,
                                                    "timestamp": datetime_obj_timestamp},
                                                    ignore_index=True)
        
        # sort by name, according to self.list_ordered_features, so to ensure that the order of the features is always this one
        features_order = CategoricalDtype(self.list_ordered_features, ordered=True)
        df_measurement["name"] = df_measurement["name"].astype(features_order)
        df_measurement.sort_values("name", inplace=True, ignore_index=True)

        return df_measurement
    
    def _measurement_to_df_and_label(self, measurement: Measurement) -> Tuple[pd.DataFrame,int]:
        """ Insert the data related to a single measurement (corresponding to a time interval)
            into a dataframe with columns = ["name", "value", "timestamp"]. Example:
        
            name          value     timestamp
        0   temperature1  21.5  2022-11-13T16:14:30.619824+00:00
        1   temperature2  21.7  2022-11-13T16:14:30.619824+00:00
        ...

        This function is used when labels are present, so get also the measurement's label separately.
        The measurement dataframe and the label are returned.

        """

        measurement_dict = measurement.dict()

        column_names = ["name", "value", "timestamp"]
        df_measurement = pd.DataFrame(columns = column_names)

        for datastream_dict in measurement_dict["Datastreams"]:

            if (datastream_dict["name"] == "label"):
                label_datastream = True
            else:
                label_datastream = False

            if datastream_dict["Observations"] != None:
                
                observations_list = datastream_dict["Observations"]

                if label_datastream == False:
                    
                    # take the mean value of the measured quantity over the considered interval
                    # keep the timestamp of the oldest observation (the utilized format for the timestamp is the ISO 8601 standard)
                    values = np.array([])
                    list_timestamp_obj = []
                    
                    for observation_dict in observations_list:
                        
                        values = np.append(values, [float(observation_dict["result"])])
                        datetime_obj = datetime.fromisoformat(observation_dict["resultTime"])
                        list_timestamp_obj.append(datetime_obj)

                    mean_value = np.mean(values)
                    datetime_obj_timestamp = min(list_timestamp_obj)
                
                else:
                    observation_dict = observations_list[0]
                    label = int(observation_dict["result"])

                    if label not in [0, 1]:
                        raise Exception("Label not valid. Labels can be only 0 or 1")
            
            else:
                datetime_obj_timestamp = None
                mean_value = None

            if label_datastream == False:
                df_measurement = df_measurement.append({"name": datastream_dict["name"],
                                                        "value": mean_value,
                                                        "timestamp": datetime_obj_timestamp},
                                                        ignore_index=True)
        
        # sort by name, according to self.list_ordered_features, so to ensure that the order of the features is always this one
        features_order = CategoricalDtype(self.list_ordered_features, ordered=True)
        df_measurement["name"] = df_measurement["name"].astype(features_order)
        df_measurement.sort_values("name", inplace=True, ignore_index=True)

        return df_measurement, label
    
    def _get_timestamp(self, df_measurement: pd.DataFrame) -> datetime:
        """ Take a dataframe related to a single measurement (corresponding to a time interval),
            i.e. a dataframe with columns = ["name", "value", "timestamp"], and extract the timestamp
            corresponding to that measurement.
        """
        
        datetime_obj_timestamp = None

        # the time interval is actually the same for the whole measurement, so we want to keep a unique timestamp.
        # The oldest valid timestamp is selected and then converted into datetime object.
        # The utilized format for the timestamp is the ISO 8601 standard.
        # Check that all timestamps in the measures are consistent
        # (within <timestamp_threshold> seconds wrt the first valid timestamp)

        list_timestamp_obj = []

        for _, row in df_measurement.iterrows():
            if row["timestamp"] == None:
                raise Exception("There is at least one missing timestamp in the current measurement")
            elif row["name"] != "label":
                datetime_obj = row["timestamp"]
                list_timestamp_obj.append(datetime_obj)
        
        datetime_obj_timestamp = min(list_timestamp_obj)

        # check timestamp consistency. All timestamps within the same measurement
        # should be within <timestamp_threshold> seconds wrt the first valid timestamp
        for curr_datetime in list_timestamp_obj:
            diff = abs(curr_datetime - datetime_obj_timestamp)
            if diff > timedelta(seconds=self.timestamp_threshold):
                raise Exception("There is at least one inconsistent timestamp in the current measurement")
            
        return datetime_obj_timestamp
    
    def _get_feature_array(self, df_measurement: pd.DataFrame) -> np.ndarray:
        """ Take a dataframe related to a single measurement (corresponding to a time interval),
            i.e. a dataframe with columns = ["name", "value", "timestamp"], and extract the feature array
            corresponding to that measurement.
        """
        
        feature_list = []
        for _, row in df_measurement.iterrows():
            if row["name"] != "label":
                feature_list.append(row["value"])
        
        feature_array = np.array(feature_list)

        return feature_array
    
    def _get_label(self, df_measurement: pd.DataFrame) -> int:
        """ Take a dataframe related to a single measurement (corresponding to a time interval),
            i.e. a dataframe with columns = ["name", "value", "timestamp"], and extract the label
            corresponding to that measurement.
        """

        # If no labels for the data are provided, they will be defined after the data will arrive.
        # Return a label whose value is:
        # 0 if all the quantities are within the limits
        # 1 if at least one sensor reports a value that exceeds the limits
        
        label = 0

        for _, row in df_measurement.iterrows():

            name = row["name"]
            value = row["value"]

            for _, row_feature in self.df_features.iterrows():
                
                if row_feature["name"] == name:
                    upper_limit = row_feature["upper_limit"]
                    lower_limit = row_feature["lower_limit"]

                    if upper_limit is not None:
                        if value >= upper_limit:
                            label = 1
                    
                    if lower_limit is not None:
                        if value <= lower_limit:
                            label = 1
                
        return label
    
    def initialize_features(self, features_list: List[dict]):
        """ The features come in a list of dictionaries, where each dict represents a feature with 3 fields:
            1) name:str
            2) upper_limit:float
            3) lower_limit:float

            Store the features' names and limits in a dataframe. Example:
            
            name          upper_limit     lower_limit
        0   temperature1  100.0           5.0
        1   temperature2  90.0            None
        ...
        """
        column_names = ["name", "upper_limit", "lower_limit"]
        df_features = pd.DataFrame(columns = column_names)
        df_features = df_features.astype('object')

        for feature_dict in features_list:
            if feature_dict["name"] != "label":
                df_features = df_features.append({"name": feature_dict["name"],
                                                    "upper_limit": feature_dict["upper_limit"],
                                                    "lower_limit": feature_dict["lower_limit"]},
                                                    ignore_index=True)
        
        # sort features in alphabetical order
        self.df_features = df_features.sort_values(by="name", ignore_index=True)
        self.list_ordered_features = self.df_features["name"].tolist()

        return

    def _check_data_integrity(self, df_measurement: pd.DataFrame):
        """ Check that df_measurement and self.df_features contain the same number of features names
            and that the names are the same.
        """

        measurement_features = df_measurement["name"].tolist()
        config_features = self.df_features["name"].tolist()

        if not sorted(measurement_features) == sorted(config_features):
            raise Exception
    
    def get_measurement_dict(self, measurement: Measurement) -> dict:
        """ Retrieve timestamp, label and feature array related to a measurement.
            Save feature_array, timestamp and label in a dictionary.
        """

        if self.is_label == False:
            
            df_measurement = self._measurement_to_dataframe(measurement)
            # get label
            try:
                label = self._get_label(df_measurement)
            except:
                logger.error("Error in retrieving label. No measurement dictionary created")
                return None
        
        else:
            df_measurement, label = self._measurement_to_df_and_label(measurement)
        
        measurement_dict = {}
            
        if df_measurement is not None:

            try:
                self._check_data_integrity(df_measurement)
            except:
                logger.error("Measurement is not consistent with features declared in configuration file")
                exit(1)

            # get timestamp
            try:
                timestamp = self._get_timestamp(df_measurement)
            except Exception as e:
                error_msg = str(e)
                logger.error(error_msg)
                return None
            
            # get feature array
            try:
                feature_array = self._get_feature_array(df_measurement)
            except:
                logger.error("Error in retrieving feature array. No measurement dictionary created")
                return None
            
        else:
            logger.error("Error in creating measurement dataframe. No measurement dictionary created")
            return None
        
        measurement_dict["feature_array"] = feature_array
        measurement_dict["timestamp"] = timestamp
        measurement_dict["label"] = label

        return measurement_dict

    
    def create_batch(self, measurements_list: List[dict], batch_size: int, n_ts_batch: int, n_ts_in: int) -> Tuple[np.ndarray, np.ndarray]:
        """ Create the feature array and the label array related to a batch with size batch_size.
            The list of measurement dictionaries that will be used to build the batch is passed as parameter.
        """

        X_array = None
        y_array = None
        series_list = []
        labels_list = []
        
        for i in range(n_ts_batch):
            
            measurement_dict = measurements_list[i]
            feature_array = measurement_dict["feature_array"]
            label = measurement_dict["label"]
            series_list.append(feature_array)
            labels_list.append(label)
                
        X_list = []
        y_list = []

        for i in range(batch_size):
            
            sample = series_list[i:i+n_ts_in]
            label_sample = labels_list[i+(n_ts_in-1)]
            X_list.append(sample)
            y_list.append(label_sample)

        # create batch features as np.ndarray
        X_array = np.array(X_list)

        # one-hot encoding of labels
        y_list_one_hot = []
        
        for item in y_list:
            if item==0:
                y_list_one_hot.append([1,0])
            elif item==1:
                y_list_one_hot.append([0,1])
            else:
                logger.error("Error in creating batch labels: incorrect label")
                return None, None
        
        # create batch labels as np.ndarray
        y_array = np.array(y_list_one_hot)
        
        return X_array, y_array

    def create_sample(self, measurements_list: List[dict], n_ts_in: int) -> np.ndarray:
        """ Create the feature array and the label array related to a batch with size batch_size.
            The list of measurement dictionaries that will be used to build the batch is passed as parameter.
        """

        X_array = None
        series_list = []
        
        for i in range(n_ts_in):
            
            measurement_dict = measurements_list[i]
            feature_array = measurement_dict["feature_array"]
            series_list.append(feature_array)
                
        X_list = []
        sample = series_list[0:n_ts_in]
        X_list.append(sample)

        # create batch features as np.ndarray
        X_array = np.array(X_list)
        
        return X_array

