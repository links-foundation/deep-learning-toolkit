import sys
import logging
import logging.config
import json
import time
import configparser
from datetime import datetime
from typing import List
import argparse
import os

sys.path.append("src/")
from DataModel import Prediction
from RedisQueue import DLTQueue
from lstm import LSTM
from data_processing import Processing


# ARG PARSER DEFINITION
parser = argparse.ArgumentParser()
parser.add_argument("--redis_host", help="Host of Redis instance", type=str, required=True)
parser.add_argument("--redis_port", help="Port of Redis instance", type=str, required=True)
parser.add_argument("--redis_pass", help="Password of Redis instance", type=str, required=True)
parser.add_argument("--log_level", help="Logging level", type=str, default="INFO")
args = parser.parse_args()
if args.log_level.upper() in ["INFO", "DEBUG", "ERROR"]:
    log_level = args.log_level.upper()
else:
    log_level = "INFO"

# LOGGING DEFINITION
logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)
logger.setLevel(log_level)


class Dlt(object):

    def __init__(self):
        """ Constructor. Create a new DLT instance.
        """

        self.measurements_queue = [] # queue that has to be filled with the new acquired measurements.
        # This queue is a List[dict], where each element of the list is a dictionary representing
        # one measurement, with 3 fields:
        # 1) feature_array:np.ndarray
        # 2) timestamp:datetime
        # 3) label:int

        self.lstm = None # LSTM model
        self.n_ts_in = 0 # number of measurements considered to build each training sample
        self.n_features = 0 # number of features considered for each measurement to build each training sample
        self.batch_size = 0 # number of samples in one batch
        self.n_ts_batch = 0 # number of measurements required to build one batch with size batch_size

        self.processing = Processing() # object for data processing

        return


    def initialize_model(self, name:str, model_dir:str, metrics_dir:str, n_ts_in:int, n_features:int, batch_size:int, load_model:bool):
        """ Initialize LSTM model either by creating a new model from scratch, with the provided parameters,
            or by loading an already existing model.
        """

        self.lstm = LSTM(name, model_dir, metrics_dir)

        path = model_dir + "/" + name

        if load_model == True:

            # search the model's folder
            is_model_dir = os.path.isdir(path)
            
            # if the model's folder already exists, load the model
            if is_model_dir == True:
                try:
                    n_ts_in_loaded, n_features_loaded = self.lstm.load_model()
                    logger.debug(f"LSTM model loaded from path: {path}")
                except:
                    logger.error(f"Could not load LSTM model from path: {path}")
                    exit(1)
            
                if ((n_ts_in_loaded!=n_ts_in) or (n_features_loaded!=n_features)):
                    n_ts_in = n_ts_in_loaded
                    n_features = n_features_loaded
                    logger.warning(f"LSTM parameters changed to fit loaded model. n_features is now: {n_features}, n_ts_in is now: {n_ts_in}")
                    logger.error("CHECK YOUR CONFIGURATION FILE FOR INCONSISTENCIES")
                else:
                    logger.info(f"LSTM model and parameters loaded. n_features is: {n_features}, n_ts_in is: {n_ts_in}")

            
            # if the model's folder does not exist, show an error
            else:
                logger.error("Model to load not found. Check model folder")
                exit(1)
            
        else:
            self.lstm.create_model(n_ts_in, n_features)
            logger.debug("New LSTM model created")

        self.n_ts_in = n_ts_in
        self.n_features = n_features
        self.batch_size = batch_size
        self.n_ts_batch = batch_size + (n_ts_in - 1)

        return
    
    
    def initialize_data_processing(self, features_list:List[dict], is_label:bool, timestamp_threshold:int):
        """ Initialize data processing by retrieving features' names and limits,
            and setting timestamp threshold to check timestamp consistency of retrieved measurements.
        """

        # retrieve features' names and limits
        self.processing.initialize_features(features_list)

        # set presence or absence of labels
        self.processing.is_label = is_label
        
        # set timestamp threshold
        self.processing.timestamp_threshold = timestamp_threshold

        return


    def train_batch(self):
        """ Create a batch with a number of samples equal to batch_size, and use it to train the model.
            At the end of the batch training, save metrics and model.
        """

        # a batch with size batch_size can be created with n_ts_batch measurements (time steps)

        measurements_list = []

        # get and remove from the queue the first n_ts_batch elements, which will be used to build a batch
        for _ in range(self.n_ts_batch):

            try:
                measurement_dict = self.measurements_queue.pop(0)
                measurements_list.append(measurement_dict)
            except:
                logger.error("Error in retrieving measurements from the Dlt measurements_queue. No batch created")
                return

        # create batch and corresponding labels as np.ndarray
        try:
            X_array, y_array = self.processing.create_batch(measurements_list, self.batch_size, self.n_ts_batch, self.n_ts_in)
            logger.debug("New batch created")
        except:
            logger.error("Error in creating batch. No batch created")
            return

        # train batch
        try:
            metrics_dict = self.lstm.train_batch(X_array, y_array)
            logger.debug(f"New batch trained. Metrics: {metrics_dict}")
        except:
            logger.error("Error in batch training. No metrics saved")
            return

        # save metrics
        try:
            self.lstm.save_metrics(metrics_dict)
            logger.debug("Metrics saved to CSV file")
        except:
            logger.error("Error in saving metrics to CSV file")

        # save model
        self.lstm.save_model()
        logger.debug("LSTM model saved")

        return


    def predict(self) -> Prediction:
        """ Create a sample with n_ts_in measurements and use it to make a new prediction.
            At the end return the new prediction.
        """

        # get the last n_ts_in elements from the measurements queue,
        # which will be used to build a sample in order to make a new prediction
        try:
            measurements_list = self.measurements_queue[-(self.n_ts_in):]
        except:
            logger.error("Error in retrieving measurements from the queue. No sample for prediction created")
            return None
        
        timestamp = measurements_list[-1]["timestamp"]

        try:
            X_array = self.processing.create_sample(measurements_list, self.n_ts_in)
        except Exception as e:
            logger.error("Error in creating sample for prediction. No prediction created")
            logger.error(e)
            return None

        logger.debug("New sample for prediction created")

        # predict
        try:
            prediction_value, prediction_precision = self.lstm.predict(X_array)
            logger.debug("New prediction successfully created")
        except Exception as e:
            logger.error("Error in making the prediction. No prediction created")
            logger.error(e)
            return None

        # create element of class Prediction
        prediction_dict = {}
        prediction_dict["result"] = prediction_value
        prediction_dict["precision"] = prediction_precision
        prediction_timestamp_string = datetime.isoformat(timestamp)
        prediction_dict["time"] =  prediction_timestamp_string
        new_prediction= Prediction(**prediction_dict)

        return new_prediction


def main_function():

    logger.info("Starting the application")

    # read configuration file
    config = configparser.ConfigParser()
    config_path = "config/dlt_config.ini"
    if os.path.isfile(config_path):
        config.read(config_path)
    else:
        logger.error("Configuration file does not exist, exiting.")
        exit(1)

    load_model = True

    n_ts_in = config["lstm"].getint("n_ts_in") 
    n_features = config["lstm"].getint("n_features")
    model_name = config["lstm"]["model_name"]

    batch_size = config["dlt"].getint("batch_size")
    time_step = config["dlt"].getint("time_step")
    n_ts_failure = config["dlt"].getint("n_ts_failure")
    max_ts_no_measurements = config["dlt"].getint("max_ts_no_measurements")
    features_file = config["dlt"]["features_file"]
    timestamp_threshold = config["dlt"].getint("timestamp_threshold")

    logger.debug("Parameters retrieved from configuration file")

    # set paths
    model_dir = "data/models"
    metrics_dir = "data/metrics"
    features_dir = "config"

    # read features file
    features_file_path = features_dir + "/" + features_file
    try:
        features_file = open(features_file_path, "r")
        features_dict = json.load(features_file)
        features_file.close()
    except:
        logger.error("Error in reading features file")

    features_list = features_dict["features"]
    is_label = False
    for feature_dict in features_list:
        if feature_dict["name"] == "label":
            is_label = True

    if is_label == True:
        n_collected_features = len(features_list) - 1
    else:
        n_collected_features = len(features_list)
    
    if n_collected_features != n_features:
        logger.error(f"Error: inconsistent number of features. Collected features:{n_collected_features}, declared features:{n_features}")
        exit(1)
    else:
        logger.debug("Features' names and limits collected from features file")


    # time_step is the sleep time for the infinite loop of the main function
    # check for new measurements and try to train a batch (if there are enough measurements) every time step
    n_trained_batches = 0

    # connect to Redis DB
    redis_client = DLTQueue(host=args.redis_host, port=args.redis_port, password=args.redis_pass)
    if redis_client.redisc == None:
        logger.error("Could not connect to Redis DB, exiting...")
        exit(1)
    
    redis_client._flush() # The DB should be empty when the DLT starts
    logger.debug("Emptied Redis DB")

    # create new DLT instance
    dlt = Dlt()
    logger.debug("New DLT instance created")

    # initialize LSTM model
    dlt.initialize_model(model_name, model_dir, metrics_dir, n_ts_in, n_features, batch_size, load_model)

    # update LSTM parameters if LSTM model has been loaded
    if load_model == True:
        n_ts_in = dlt.n_ts_in
        n_features = dlt.n_features
    
    logger.debug(f"LSTM initialized with parameters: n_ts_in={n_ts_in}, n_features={n_features}, batch_size={batch_size}")

    # initialize data processing
    dlt.initialize_data_processing(features_list, is_label, timestamp_threshold)

    # initialize counter to count consecutive time steps in which
    # no measurements are retrieved from Redis DB
    counter_ts_no_measurements = 0
    sleep_mode = False


    while True:

        # get measurements from Redis DB
        retrieved_measurements = redis_client.getMeasurementsDLT()
        logger.debug(f"{len(retrieved_measurements)} measurements retrieved from Redis DB")

        # update counter: if no measurements are retrieved, the counter is incremented.
        # If some measurements are retrieved, the counter is reset
        if len(retrieved_measurements) == 0:
            counter_ts_no_measurements = counter_ts_no_measurements + 1
            logger.debug(f"No measurements received for {counter_ts_no_measurements} time steps")
        else:
            counter_ts_no_measurements = 0
            # deactivate sleep mode if it was activated
            if sleep_mode == True:
                logger.info("New measurements retrieved from Redis DB. Exiting sleep mode...")
                sleep_mode = False
        
        # check counter: if too many time steps have passed since the last measurement
        # was received, do not perform any predictions
        if counter_ts_no_measurements >= max_ts_no_measurements:
            
            # activate sleep mode if it was not activated
            if sleep_mode == False:
                sleep_mode = True
                logger.info("No measurements are being retrieved from Redis DB. Entering sleep mode...")
                # empty measurements queue, because the measurements still in the queue
                # cannot be put in the same batch with measurements that will arrive later in time
                dlt.measurements_queue = []
                
            # wait for a number of seconds equal to time_step
            time.sleep(time_step)
            continue

        # iterate over the list of retrieved measurements
        for measurement in retrieved_measurements:
            
            # get measurement_dict with measurement, timestamp, label
            measurement_dict = dlt.processing.get_measurement_dict(measurement)
            if measurement_dict == None:
                logger.error("Error in creating measurement dictionary with measurement, timestamp, label")
                continue
            
            dlt.measurements_queue.append(measurement_dict)
            
            # if the label is 1 (failure), update the previous (n_ts_failure-1) measurements so that they get label 1.
            # Since we want to predict the probability to have a failure (label 1) in n_ts_failure time steps,
            # the (n_ts_failure-1) measurements before a failure should be marked with label 1 too
            if measurement_dict["label"] == 1:
                if len(dlt.measurements_queue) >= (n_ts_failure-1):
                    list_to_update = dlt.measurements_queue[-(n_ts_failure-1):]
                    for measurement_item in list_to_update:
                        measurement_item["label"] = 1
                else:
                    list_to_update = dlt.measurements_queue
                    for measurement_item in list_to_update:
                        measurement_item["label"] = 1
                
        logger.debug(f"Retrieved measurements appended to Dlt queue. Dlt queue length is now: {len(dlt.measurements_queue)}")

        # if there are enough measurements, create a batch and use it to train the LSTM model.
        # To ensure that there are always at least (n_ts_failure - 1) measurements in the DLT queue,
        # train a batch only if there are at least (n_ts_batch + n_ts_failure - 1) measurements, because
        # n_ts_batch measurements will be removed after the training
        if len(dlt.measurements_queue) >= (dlt.n_ts_batch + n_ts_failure - 1):
            logger.info("Creating and training new batch of data")
            dlt.train_batch()
            n_trained_batches = n_trained_batches + 1
            logger.debug("End of batch training")
        else:
            logger.info("Not enough measurements in Dlt queue to train a batch. Waiting for more measurements...")
        
        # new prediction.
        # The prediction is made on data that has not been used for training yet.
        # If n_ts_in <= (n_ts_failure - 1), it is possible to obtain a prediction at every iteration
        if n_trained_batches > 0 or load_model:
            
            if len(dlt.measurements_queue) >= dlt.n_ts_in:
                
                # make new prediction
                logger.debug("Making new prediction")
                new_prediction = dlt.predict()
                new_prediction_dict = new_prediction.dict()
                logger.info(f"New prediction: {new_prediction_dict}")

                # add new prediction to Redis DB
                try:
                    redis_client.addPrediction(json.dumps(new_prediction_dict))
                    logger.debug("New prediction added on Redis DB")
                except:
                    logger.error("Error in adding the new prediction to Redis DB")
            else:
                logger.info("Not enough measurements in Dlt queue to make a prediction. Waiting for more measurements...")
        else:
            logger.info("Batch training not started yet. No prediction made, waiting for more measurements...")

        # wait for a number of seconds equal to time_step
        time.sleep(time_step)
        

if __name__ == '__main__':

    main_function()
