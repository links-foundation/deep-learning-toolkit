from pydantic import BaseModel


class Reading(BaseModel):
    sensor: str
    timestamp: str
    result: str