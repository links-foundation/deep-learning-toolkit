from pydantic import BaseModel
from typing import List, Optional


class FeatureOfInterest(BaseModel):
    name: str
    description: str
    encodingType: str
    feature: Optional[str]


class Observation(BaseModel):
    phenomenonTime: str
    resultTime: str
    result: str
    resultQuality: Optional[List[str]]
    validTime: Optional[str]
    parameters: Optional[List[dict]]
    FeatureOfInterest: Optional[FeatureOfInterest]


class Datastream(BaseModel):
    name: str
    description: str
    observationType: str
    unitOfMeasurement: dict
    observedArea: Optional[str]
    phenomenonTime: Optional[str]
    resultTime: Optional[str]
    Observations: Optional[List[Observation]]
    ObservedProperty: dict
    Sensor: dict


class OGCST(BaseModel):
    name: str
    description: str
    properties: Optional[dict]
    Locations: Optional[List[dict]]
    HistoricalLocations: Optional[List[dict]]
    Datastreams: Optional[List[Datastream]]


class Measurement(OGCST):
    pass
