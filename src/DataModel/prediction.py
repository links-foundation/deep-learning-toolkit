from pydantic import BaseModel


class Prediction(BaseModel):
    result: int
    precision: float
    time: str