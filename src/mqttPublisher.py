import logging
import time
import logging.config
import paho.mqtt.client as mqtt
import argparse
import traceback
import random
import string
import os

from RedisQueue import DLTQueue


# LOGGING DEFINITION
logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


# ARG PARSER DEFINITION
parser = argparse.ArgumentParser()
parser.add_argument("--broker_host", help="Host of the MQTT broker", type=str, required=True)
parser.add_argument("--broker_port", help="Port of the MQTT broker", type=int, required=True)
parser.add_argument("--topic_out", help="Topic on which publish the data", type=str, required=True)
parser.add_argument("--broker_user", help="Username for the MQTT broker", type=str, required=True)
parser.add_argument("--broker_pass", help="Password for the MQTT broker", type=str, required=True)
parser.add_argument("--redis_host", help="Host of Redis instance", type=str, required=True)
parser.add_argument("--redis_port", help="Port of Redis instance", type=str, required=True)
parser.add_argument("--redis_pass", help="Password of Redis instance", type=str, required=True)
parser.add_argument("--flush_redis", help="Flush Redis queues on startup", type=bool, required=False, default=False)
args = parser.parse_args()

# MQTT CLIENT DEFINTION
class MQTTClient:

    def __init__(self):
        client_id = 'DLT_'.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        self.mqttc = mqtt.Client(client_id=client_id, clean_session=False)

        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_disconnect = self.on_disconnect
        self.mqttc.on_publish = self.on_publish

        self.brokerHost = args.broker_host
        self.brokerPort = args.broker_port
        self.topicOut = args.topic_out

        self.certPath = "certs/cacert.pem"

        if self.brokerHost is None:
            logger.error("No broker host defined, exiting.")
            exit(1)
        if self.brokerPort is None:
            logger.error("No broker port defined, exiting.")
            exit(1)
        if self.topicOut is None:
            logger.error("No topic to subscribe defined, exiting.")
            exit(1)

    def connect(self):
        if self.brokerHost != "127.0.0.1": # if the broker is 127.0.0.1 then assume debug setup
            if not os.path.isfile(self.certPath):
                logger.error("Missing broker certificate file 'certs/cacert.pem'")
                exit(1)
            self.mqttc.username_pw_set(username=args.broker_user,password=args.broker_pass)
            self.mqttc.tls_set(ca_certs="certs/cacert.pem")
        self.mqttc.connect(self.brokerHost, self.brokerPort, keepalive=60)

    def reconnect(self):
        self.mqttc.connect(self.brokerHost, self.brokerPort, keepalive=60)

    def on_disconnect(self, client, userdata, rc):
        logger.warn(f"Client disconnected: RC {rc}")
        reconnected = False

        while not reconnected:
            try:
                self.reconnect()
                reconnected = True
            except:
                logger.error("Could not reconnect")
                logger.error(traceback.format_exc())
                time.sleep(120)

    def on_connect(self, mqttc :mqtt.Client, obj, flags, rc):
        logger.info(f"Connected to: Host:{self.brokerHost}\tPort:{self.brokerPort}\tTopic:{self.topicOut}\tRC:{rc}")

    def on_publish(self, client, obj, mid):
        logger.info("Message published")

    def publish(self, payload:str):
        try:
            self.mqttc.publish(self.topicOut, payload, qos=2)
        except:
            logger.error("Failed to publish message")


# MAIN
def main():
    mqttClient = MQTTClient()
    mqttClient.connect()

    dltQueue = DLTQueue(host=args.redis_host, port=args.redis_port, password=args.redis_pass)

    while True:
        prediction = dltQueue.getPredictionMQTT()
        mqttClient.mqttc.loop_start()
        if prediction is None:
            time.sleep(0.5)
        else:
            mqttClient.publish(prediction)
        mqttClient.mqttc.loop_stop()


if __name__ == "__main__":
    main()
