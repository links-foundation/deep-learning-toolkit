import logging
import requests
import paho.mqtt.client as mqtt
import argparse
import random
import string
import os

from RedisQueue import DLTQueue


# LOGGING DEFINITION
logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


# ARG PARSER DEFINITION
parser = argparse.ArgumentParser()
parser.add_argument("--broker_host", help="Host of the MQTT broker", type=str, required=True)
parser.add_argument("--broker_port", help="Port of the MQTT broker", type=int, required=True)
parser.add_argument("--topic", help="Topic on which receive the data", type=str, required=True)
parser.add_argument("--broker_user", help="Username for the MQTT broker", type=str, required=True)
parser.add_argument("--broker_pass", help="Password for the MQTT broker", type=str, required=True)
parser.add_argument("--redis_host", help="Host of Redis instance", type=str, required=True)
parser.add_argument("--redis_port", help="Port of Redis instance", type=str, required=True)
parser.add_argument("--redis_pass", help="Password of Redis instance", type=str, required=True)
parser.add_argument("--flush_redis", help="Flush Redis queues on startup", type=bool, required=False, default=False)
args = parser.parse_args()


# MQTT CLIENT DEFINTION
class MQTTClient:

    def __init__(self):
        client_id = 'DLT_'.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        self.dltQueue = DLTQueue(host=args.redis_host, port=args.redis_port, password=args.redis_pass)
        self.mqttc = mqtt.Client(client_id=client_id, clean_session=False)

        self.mqttc.on_message = self.on_message
        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_subscribe = self.on_subscribe

        self.brokerHost = args.broker_host
        self.brokerPort = args.broker_port
        self.topicIn = args.topic

        self.certPath = "certs/cacert.pem"

        if self.brokerHost is None:
            logger.error("No broker host defined, exiting.")
            exit(1)
        if self.brokerPort is None:
            logger.error("No broker port defined, exiting.")
            exit(1)
        if self.topicIn is None:
            logger.error("No topic to subscribe defined, exiting.")
            exit(1)

    def connect(self):
        if self.brokerHost != "127.0.0.1": # if the broker is 127.0.0.1 then assume debug setup
            if not os.path.isfile(self.certPath):
                logger.error("Missing broker certificate file 'certs/cacert.pem'")
                exit(1)
            self.mqttc.username_pw_set(username=args.broker_user,password=args.broker_pass)
            self.mqttc.tls_set(ca_certs=self.certPath)
        self.mqttc.connect(self.brokerHost, self.brokerPort, 60)
        if args.flush_redis:
            self.dltQueue._flush()

    def subscribe(self):
        self.mqttc.subscribe(self.topicIn, 0)

    def on_connect(self, mqttc :mqtt.Client, obj, flags, rc):
        logger.info(f"Connected to: Host:{self.brokerHost}\tPort:{self.brokerPort}\tTopic:{self.topicIn}\tRC:{rc}")
        self.subscribe()

    def on_message(self, mqttc :mqtt.Client, obj, msg):
        """ Add measurement to be consumed by the DLT
        """
        logger.info(f"Received message on topic:{msg.topic} with qos:{msg.qos}")
        try:
            payload = msg.payload.decode("utf-8")
            self.dltQueue.addMeasurementDLT(payload)
            logger.info("Stored Measurement")
        except Exception as e:
            logger.error(e, "Failed to store measurement")
        return

    def on_subscribe(self, mqttc :mqtt.Client, obj, mid, granted_qos):
        logger.info(f"Subscribed: {mid}, {granted_qos}")


# MAIN
def main():
    mqttClient = MQTTClient()
    mqttClient.connect()
    mqttClient.mqttc.loop_forever()


if __name__ == "__main__":
    main()
