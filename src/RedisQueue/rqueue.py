import redis
import pickle
import json
import logging
import logging.config

from typing import List

import sys
sys.path.append("src/")
from DataModel import Measurement, Reading, Prediction

# LOGGING DEFINITION
logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)

class DLTQueue:

    def __init__(self, host, port, password):
        self.nStoredItems = 1000 # TODO we should discuss this queue size
        self.host = str(host)
        self.port = int(port)
        self.password = str(password)
        self.connect()


    def connect(self) -> bool:
        try:
            pool = redis.BlockingConnectionPool(host=self.host, port=self.port, max_connections=1, password=self.password)
            self.redisc = redis.StrictRedis(connection_pool=pool)
            self.redisc.ping()
        except Exception as e:
            self.redisc = None
            logger.error(e)
            logger.error(f"Can't connect to redis: host={self.host} port={self.port} pass={self.password}")
            return False

        logger.info(f"Connected to redis: host={self.host} port={self.port}")
        return True

    def _flush(self):
        self.redisc.flushall()
        logger.warning("Flushed Redis DB")

    def _put(self, queue :str, payload :str):
        """
        """
        try:
            # push values in "payload" onto the tail of the list "queue"
            self.redisc.rpush(queue, pickle.dumps(payload))
            # cut the list "queue", so that only the last "nStoredItems" elements remain
            self.redisc.ltrim(queue, -self.nStoredItems, -1)
            #logger.info(f"Successfully stored item in queue {queue}")
        except Exception as e:
            logger.error(e, f"Failed to store item in queue {queue}")

    def _getReading(self, queue :str) -> List[Reading]:
        items = None
        rawItems = None
        try:
            # fetch from queue, item is None if queue is empty
            # self.redisc.lrange("queue name", "how many previous items we get", "latest item we get")
            rawItems = self.redisc.lrange(queue, -self.nStoredItems, -1)
        except Exception as e:
            logger.error(e, f"Failed to load item from queue {queue}")

        if rawItems is not None:
            if len(rawItems) > 0:
                items = list()
                try:
                    for meas in rawItems:
                        items.append(Reading.parse_obj(json.loads(pickle.loads(meas))))
                except Exception as e:
                    logger.error(e, f"Error while unmarshaling item from queue {queue}")
        else:
            logger.error(f"Empty item loaded from queue {queue}")

        return items


    def _getPrediction(self, queue :str) -> List[Prediction]:
        items = None
        rawItems = None
        try:
            # fetch from queue, item is None if queue is empty
            # self.redisc.lrange("queue name", "how many previous items we get", "latest item we get")
            rawItems = self.redisc.lrange(queue, -self.nStoredItems, -1)
        except Exception as e:
            logger.error(e, f"Failed to load item from queue {queue}")

        if rawItems is not None:
            if len(rawItems) > 0:
                items = list()
                try:
                    for meas in rawItems:
                        items.append(Prediction.parse_obj(json.loads(pickle.loads(meas))))
                    logger.info("Loaded Measurement")
                except Exception as e:
                    logger.error(e, f"Error while unmarshaling item from queue {queue}")
        else:
            logger.error(f"Empty item loaded from queue {queue}")

        return items

    def _getMeasurement(self, queue :str) -> List[Measurement]:
        """ Get and remove up to 100 elements from Redis DB queue "queue".
        """
        items = list()
        rawItems = list()
        try:
            for _ in range(100):
                pop_element = self.redisc.lpop(queue)
                if pop_element != None:
                    rawItems.append(pop_element)
        except Exception as e:
                logger.error(e, f"Failed to pop item from queue {queue}")

        if len(rawItems)!=0:
            try:
                for meas in rawItems:
                    items.append(Measurement.parse_obj(json.loads(pickle.loads(meas))))
                logger.info("Loaded Measurements")
            except Exception as e:
                logger.error(e, f"Error while unmarshaling item from queue {queue}")
        else:
            logger.error(f"Empty item loaded from queue {queue}")

        return items

    def _isQueue(self, queue:str) -> bool:
        """ Check if a queue exists in Redis DB
        """
        is_queue = self.redisc.exists(queue)
        return is_queue

    def addMeasurementUI(self, measurement :str) -> None:
        """ Store the last 100 measurements into redis
        """
        raise NotImplementedError("This method will be implemented in the future")

    def addPrediction(self, prediction :str):
        prediction = Prediction.parse_obj(json.loads(prediction))
        self._put("predictions_ui", prediction.json())
        self._put("predictions_mqtt", prediction.json())

    def getMeasurementUI(self, *args, **kwargs):
        raise NotImplementedError("This method will be implemented in the future")

    def getPredictionReadingsUI(self) -> List[Prediction]:
        readings = self._getPrediction("predictions_ui")
        return readings

    def getPredictionMQTT(self) -> Prediction:
        queue = "predictions_mqtt"
        prediction = None
        rawItems = None
        try:
            # fetch from queue, item is None if queue is empty
            # self.redisc.lrange("queue name", "how many previous items we get", "latest item we get")
            rawItems = self.redisc.lrange(queue, -self.nStoredItems, -1)
            self.redisc.delete(queue)
        except Exception as e:
            logger.error(e, f"Failed to load item from queue {queue}")

        if rawItems is not None:
            if len(rawItems) > 0:
                try:
                    prediction = pickle.loads(rawItems[-1])
                    logger.info("Loaded Measurement")
                except Exception as e:
                    logger.error(e, f"Error while unmarshaling item from queue {queue}")
        else:
            logger.error(f"Empty item loaded from queue {queue}")

        return prediction

    def addMeasurementDLT(self, measurement :str) -> None:
        """ Store measurement into redis
        """
        try:
            measurement = Measurement.parse_obj(json.loads(measurement))
        except:
            logger.error("Received data with a wrong format")
            return
        self._put("measurements_dlt", measurement.json())

        return

    def getMeasurementsDLT(self) -> List[Measurement]:
        measurements = []
        if self._isQueue("measurements_dlt"):
            measurements = self._getMeasurement("measurements_dlt")
        else:
            logger.warning("measurements_dlt queue not initialized in Redis DB (no measurement pushed yet?)")

        return measurements
