import argparse
import logging
import os
import shutil
from pathlib import Path
from configparser import ConfigParser

import docker
import requests
from fastapi import Depends, FastAPI, File, HTTPException, UploadFile
from fastapi.responses import FileResponse, Response
from fastapi.security import HTTPBasic, HTTPBasicCredentials

parser = argparse.ArgumentParser()
parser.add_argument('--container_name', help='foo help',
                    default=os.environ.get("DLT_CONTAINER_NAME"), required=False)
parser.add_argument('--asg_url', help='foo help',
                    default=os.environ.get("ASG_URL"), required=False)
parser.add_argument('--client_secret', help='foo help',
                    default=os.environ.get("ASG_CLIENT_SECRET"), required=False)

args, unknown = parser.parse_known_args()


app = FastAPI()
security = HTTPBasic()
logger = logging.Logger(name=__name__)


def checkAuthorized(credentials):
    url = args.asg_url
    body = {
        "grant_type": "password",
        "client_id": "apisix",
        "client_secret": args.client_secret,
        "username": credentials.username,
        "password": credentials.password
    }
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    r = requests.post(url=url, headers=headers, data=body)
    if r.status_code == 401:
        logger.error(msg="Not authorized")
        return False
    try:
        logger.debug(msg="Token obtained")
        r.json()["access_token"]
        return True
    except:
        logger.error(msg="Failed to obtain token")
        return False


@app.get("/info")
def get_demo(credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        client = docker.from_env()
        container = client.containers.get(args.container_name)
        logs = container.attach(logs=True).decode("utf-8").splitlines()
        print(type(logs))
        return logs[-20:]
    except:
        raise HTTPException(status_code=404, detail="The API was not correctly configured or the DLT isn't running")


@app.get("/start")
def start_container(credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        client = docker.from_env()
        container = client.containers.get(args.container_name)
        container.start()
        return Response(status_code=200)
    except Exception as e:
        print("errore start", e)
        raise HTTPException(status_code=500)


@app.get("/stop")
def stop_container(credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        client = docker.from_env()
        container = client.containers.get(args.container_name)
        container.stop()
        return Response(status_code=200)
    except Exception as e:
        print("errore stop", e)
        raise HTTPException(status_code=500)


@app.post("/model")
def upload_model(model_name: str, model: UploadFile = File(...), credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        model_dir = os.getcwd() + "/data/models/" + \
            os.path.basename(os.path.normpath(model_name))
        Path(model_dir).mkdir(parents=False, exist_ok=True)
        file_name = model_dir + "/" + model.filename.replace(" ", "-")
        with open(file_name, 'wb+') as f:
            f.write(model.file.read())
            f.close()
        return Response(status_code=200)
    except Exception as e:
        print("errore upload", e)
        raise HTTPException(status_code=500)

@app.post("/config")
def upload_config(config_file: UploadFile = File(...), credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        config_file_dir = os.getcwd() + "/config"
        file_name = config_file_dir + "/" + config_file.filename.replace(" ", "-")
        with open(file_name, 'wb+') as f:
            f.write(config_file.file.read())
            f.close()
        return Response(status_code=200)
    except Exception as e:
        print("errore upload", e)
        raise HTTPException(status_code=500)


@app.get("/model")
def download_model(model_name: str, file_name: str, credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        file_path = os.getcwd() + "/data/models/" + \
            os.path.basename(os.path.normpath(model_name)) + \
            "/" + os.path.basename(os.path.normpath(file_name))
        print(file_path)
        return FileResponse(path=file_path, filename=file_name)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)


@app.get("/config")
def download_config(file_name: str, credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        file_path = os.getcwd() + "/config" + \
            "/" + os.path.basename(os.path.normpath(file_name))
        print(file_path)
        return FileResponse(path=file_path, filename=file_name)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)


@app.delete("/model")
def remove_model(model_name: str, credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        model_dir = os.getcwd() + "/data/models/" + \
            os.path.basename(os.path.normpath(model_name)) 
        shutil.rmtree(model_dir)
        return Response(status_code=200)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)


@app.delete("/config")
def remove_config(file_name: str, credentials: HTTPBasicCredentials = Depends(security)):
    if not checkAuthorized(credentials):
        raise HTTPException(status_code=401)
    try:
        config_file = os.getcwd() + "/config/" + \
            os.path.basename(os.path.normpath(file_name)) 
        print(config_file)
        os.remove(config_file)
        return Response(status_code=200)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)