import logging
import logging.config
import time
import json
import paho.mqtt.client as mqtt
import argparse
import traceback
import datetime
import string
import random
import sys

sys.path.append("src")
from DataModel import Measurement, Datastream, Observation

# LOGGING DEFINITION
logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


# ARG PARSER DEFINITION
parser = argparse.ArgumentParser()
parser.add_argument("--broker_host", help="Host of the MQTT broker", type=str, required=True)
parser.add_argument("--broker_port", help="Port of the MQTT broker", type=int, required=True)
parser.add_argument("--topic_out", help="Topic on which publish the data", type=str, required=True)
parser.add_argument("--broker_user", help="Username for the MQTT broker", type=str, required=True)
parser.add_argument("--broker_pass", help="Password for the MQTT broker", type=str, required=True)
args = parser.parse_args()

# MQTT CLIENT DEFINTION
class MQTTClient:

    def __init__(self):
        client_id = 'DLT_'.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        self.mqttc = mqtt.Client(client_id=client_id, clean_session=False)

        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_disconnect = self.on_disconnect
        self.mqttc.on_publish = self.on_publish

        self.brokerHost = args.broker_host
        self.brokerPort = args.broker_port
        self.topicOut = args.topic_out

        if self.brokerHost is None:
            logger.error("No broker host defined, exiting.")
            exit(1)
        if self.brokerPort is None:
            logger.error("No broker port defined, exiting.")
            exit(1)
        if self.topicOut is None:
            logger.error("No topic to subscribe defined, exiting.")
            exit(1)

    def connect(self):
        if self.brokerHost != "127.0.0.1": # if the broker is 127.0.0.1 then assume debug setup
            self.mqttc.username_pw_set(username=args.broker_user,password=args.broker_pass)
            self.mqttc.tls_set(ca_certs="certs/cacert.pem")
        self.mqttc.connect(self.brokerHost, self.brokerPort, keepalive=60)

    def reconnect(self):
        self.mqttc.connect(self.brokerHost, self.brokerPort, keepalive=60)

    def on_disconnect(self, client, userdata, rc):
        logger.warn(f"Client disconnected: RC {rc}")
        reconnected = False

        while not reconnected:
            try:
                self.reconnect()
                reconnected = True
            except:
                logger.error("Could not reconnect")
                logger.error(traceback.format_exc())
                time.sleep(120)

    def on_connect(self, mqttc :mqtt.Client, obj, flags, rc):
        logger.info(f"Connected to: Host:{self.brokerHost}\tPort:{self.brokerPort}\tTopic:{self.topicOut}\tRC:{rc}")

    def on_publish(self, client, obj, mid):
        logger.info("Message published")

    def publish(self, payload:str):
        try:
            self.mqttc.publish(self.topicOut, payload, qos=2)
            # logger.info(f"Published '{payload}' to '{self.topicOut}'")
        except:
            logger.error("Failed to publish message")


def generate_message(filename: str):
    with open(filename, "r") as fp:
        message = fp.read()

    measurement = Measurement(
        name="Test Measurement",
        description="Test measurement"
    )

    features = json.loads(message)

    datastreams = []


    for j, sensor in enumerate(features["features"]):

        j=0
        data_timestamp = datetime.datetime.now()
        data_timestamp = data_timestamp.replace(microsecond=0).isoformat()
        observations = []

        if sensor["name"] == "label":
            timestamp = datetime.datetime.now()
            timestamp = timestamp.replace(microsecond=0).isoformat()
            observation = Observation(
                phenomenonTime=timestamp,
                resultTime=timestamp,
                result=str(0)
            )
            observations.append(observation)
        
        else:
            for i in reversed(range(5)):
                timestamp = datetime.datetime.now()
                timestamp = timestamp.replace(microsecond=0).isoformat()
                observation = Observation(
                    phenomenonTime=timestamp,
                    resultTime=timestamp,
                    result=str(random.randint(0,100))
                )
                observations.append(observation)

        datastream = Datastream(
            name=sensor["name"],
            description="Example sensor",
            observationType="N/A",
            unitOfMeasurement={},
            resultTime=data_timestamp,
            Observations=observations,
            ObservedProperty={},
            Sensor={}

        )

        datastreams.append(datastream)
    
    measurement.Datastreams = datastreams

    return measurement.json()


# MAIN
def main():

    mqttClient = MQTTClient()
    mqttClient.connect()


    while True:
        mqttClient.mqttc.loop_start()
        message = generate_message("test/data/example_with_label.json")
        mqttClient.publish(message)
        mqttClient.mqttc.loop_stop()
        time.sleep(5)


if __name__ == "__main__":
    main()
