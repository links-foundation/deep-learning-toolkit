# Deep Learning Toolkit

## How to build and run

The Deep Learning Toolkit has been tested on a system running **Docker version 20.10** and **docker-compose version 1.28**.

To run the Deep Learning Toolkit just execute the command `docker-compose up  --build --force-recreate` after creating the `.env` file as described in the *Environment variables file* section below.

## API Schemas

The Deep Learning Toolkit has a REST API which can be used to start and stop the predictive maintenance module, as well as upload and download the models and the configuration files. The complete documentation of the API in OpenAPI format is available [here](https://editor.swagger.io/?url=https://bitbucket.org/links-foundation/deep-learning-toolkit/raw/b3dc7d2e047bbafbce3e20c8dd84a496e2cdaaf8/schemas/restapi.json).

The DLT uses the MQTTS protocol for input and output operations. The AsyncAPI schema for input data is available [here](https://studio.asyncapi.com/?url=https://bitbucket.org/links-foundation/deep-learning-toolkit/raw/ae8626aed13a99f38a7a1e06fb2bf30c953b85be/schemas/input.yaml) while [here](https://studio.asyncapi.com/?url=https://bitbucket.org/links-foundation/deep-learning-toolkit/raw/ae8626aed13a99f38a7a1e06fb2bf30c953b85be/schemas/output.yaml) you can find the schema for the output data, i.e. the predictions.

## Configuration files

### DLT Configuration file

Content of the `dlt_config.ini` configuration file to be created in the `config/` folder:
    
    [lstm]
    n_ts_in = 10
    n_features = 18
    model_name = example_model

    [dlt]
    batch_size = 32
    time_step = 10
    n_ts_failure = 120
    max_ts_no_measurements = 30
    features_file = example_features.json
    timestamp_threshold = 120

The configuration file variables act on these parameters:

- LSTM
  - `n_ts_in` (integer): number of time steps of the time series that are used to perform training and prediction
  - `n_features` (integer): number of features, i.e. sensor values, given as input to the LSTM
  - `model_name` (string): name of the LSTM model that has to be loaded

- DLT
  - `batch_size` (integer): size of each batch used to train LSTM models
  - `time_step` (integer): number of seconds corresponding to the interval between two consecutive measurements
  - `n_ts_failure` (integer): number of seconds corresponding to the time window within which the failure is predicted
  - `max_ts_no_measurements` (integer): number of time steps with no measurements received that have to pass to make the DLT activate the sleep mode
  - `features_file` (string): name of the file containing the names and limits of the features, i.e. sensor values
  - `timestamp_threshold` (integer): number of seconds defining a time window which the sensor values belonging to the same measurement must be within, in order for the measurement to be valid

### Environment variables file

Content of the `.env` file to be created in the project root, containing the parameters required by the docker-compose configuration file: 

    BROKER_HOST=<host of the EFPF MQTT broker>
    BROKER_PORT=<port of the EFPF MQTT broker>
    BROKER_USER=<username enabled on the MQTT broker (use your EFPF account)>
    BROKER_PASS=<password enabled on the MQTT broker (use your EFPF account)>
    REDIS_HOST=<host on which the Redis container will be deployed (usually the host of the DLT)>
    REDIS_PORT=<port you wish to expose Redis service on>
    REDIS_PASS=<password you choose for Redis, make it strong!>
    INPUT_DATA_TOPIC=<MQTT topic on which the DLT will receive data>
    OUTPUT_DATA_TOPIC=<MQTT topic on which the DLT will publish data>
    API_HOST=<host on which you want to publish the HTTP rest API for controlling the DLT>
    API_PORT=<port on which you want to publish the HTTP rest API for controlling the DLT>
    DLT_CONTAINER_NAME=<name of the container of the dlt, required for the API to work>
    ASG_CLIENT_SECRET=<EFPF OIDC client secret>
    ASG_URL=<EFPF API Security Gateway URL>

### Features file

Example of the content of the json file to be uploaded in the config/ folder:

`{
    "features": [
        {
            "name": "sensor1",
            "upper_limit": 20,
            "lower_limit": null
        },
        {
            "name": "sensor2",
            "upper_limit": 10,
            "lower_limit": 0
        }
     ]
}`

In this file the fileld "*name*" of the feature corresponds to the field "*name*" of the corresponding Datastream in the OGC-SensorsThings formatted input data.

The number of features to be inserted in the json file must be equal to the value of *n_features* parameter that has been set in the `dlt_config.ini` file. The *upper_limit* and the *lower_limit* define the range of values within which each feature should be during normal operation conditions; values outside that range are considered anomalous. When an *upper_limit* or a *lower_limit* are not provided, it is necessary to set them as *null* in the json file.

If labels are provided inside the input data, as a Datastream with the field name set as *label*, an additional feature has to be added in the json file, with *upper_limit* and *lower_limit* set as null, as in the following example:

`{
    "features": [
        {
            "name": "sensor1",
            "upper_limit": 20,
            "lower_limit": null
        },
        {
            "name": "sensor2",
            "upper_limit": 10,
            "lower_limit": 0
        },
        {
            "name": "label",
            "upper_limit": null,
            "lower_limit": null
        }
     ]
}`

Example files are provided in the examples folder.

